from PIL import Image, ImageDraw, ImageFont
image = Image.open("1_.jpg")
text = "e"
font = ImageFont.truetype("C:\Windows\Fonts\Ebrima.ttf", 1000)
layer = image.convert("l")   # 将图像转为RGBA格式

# 生成相同大小的图片
text_overlay = Image.new("l", layer.size, (255, 255, 255, 50))
image_draw = ImageDraw.Draw(text_overlay)

# 获取文本大小
text_size_X, text_size_Y = image_draw.textsize(text, font=font)

# 设置文本位置
text_xy = (layer.size[0]- text_size_X, layer.size[1]-text_size_Y)
# 设置文本颜色和透明度和位置
# image_draw.text(text_xy,text,font=font,fill=(255, 255, 255, 140))

# 将新生成的图片覆盖到需要加水印的图片上

after = Image.alpha_composite(layer, text_overlay)
after.save("im_after1.png")
