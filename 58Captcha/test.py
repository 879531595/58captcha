from tensorflow import image
import matplotlib.pyplot as plt
import tensorflow as tf

image_raw_data = tf.gfile.FastGFile("1.png", 'rb').read()

with tf.Session() as sess:
    img_data = tf.image.decode_png(image_raw_data)
    img_data2 = tf.image.adjust_hue(img_data, -0.9)
    plt.imshow(img_data2.eval())
    plt.show()

# image.adjust_brightness()