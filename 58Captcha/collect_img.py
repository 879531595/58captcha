import time
import requests
import re
import os
import traceback
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)




def get_t():
    return str(int(time.time()) * 1000)

class Collect_img():

    headers = {
        "Host": "verifycode.58.com",
        "Connection": "keep-alive",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "zh-CN,zh;q=0.9",

    }
    baseurl = "https://verifycode.58.com/captcha/getV3?callback=jQuery110108833127385895849_1552626824671&showType=win&sessionId={}&_={}"

    sessionId = ""

    img_total = 1000

    img_dir = os.path.join(os.path.dirname(__file__), "imgs")

    def __init__(self):
        self._check_img_dir()
        self.Refresh_sessionId()
        self.collect_captcha_imgs()


    @property
    def obj_url(self):
        return self.baseurl.format(self.sessionId, get_t())


    def collect_captcha_imgs(self):

        for i in range(self.img_total):
            try:
                print("collect no: ", i + 1)

                response = requests.get(self.obj_url, headers=self.headers, verify=False)
                sour = response.text
                if "成功" in sour:
                    obj_url = re.search('"bgImgUrl":"(.*?)&it=_big"}', sour)
                    if obj_url:
                        info = obj_url.group(1).strip()
                        _url = "https://verifycode.58.com" + info + "&it=_big"
                        self.save_img(_url)
                        print("success: collect no: ", i + 1)
                elif "请求失败，请重试" in sour or "您获取图片的次数太多了,请刷新页面" in sour:
                    self.Refresh_sessionId()
            except Exception as ex:
                print(traceback.format_exc())



    def Refresh_sessionId(self):
        refresh_url = "https://callback.58.com/firewall/codev2/getsession.do?1552631354641"

        refresh_headers = {
                           "Host": "callback.58.com",
                           "Connection": "keep-alive",
                           "Accept": "*/*",
                           "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
                           "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
                           "Accept-Encoding": "gzip, deflate, br",
                           "Accept-Language": "zh-CN,zh;q=0.9",
                           }

        post_data = {
            "serialId": "d22d6f319b5148ae8a199a4b574dd8b4_7daf4f969cac4852a2fbbfb2e275bbcd",
            "code": "22",
            "sign": "26dbf4d672a5d9691e63e40bb02910d0",
            }
        response = requests.post(refresh_url,headers=refresh_headers,json=post_data, verify=False)
        json_data = response.json()
        self.sessionId = json_data["data"]["sessionId"]

    def _check_img_dir(self):
        if not os.path.exists(self.img_dir):
            os.makedirs(self.img_dir)


    def save_img(self, url):
        img_headers = {
            "Host": "verifycode.58.com",
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "zh-CN,zh;q=0.9",
        }

        response = requests.get(url, headers=img_headers, verify=False)
        obj_path = os.path.join(self.img_dir, "dome_%s.png" % get_t())
        with open(obj_path, "wb") as f:
            f.write(response.content)




if __name__ == '__main__':
    Collect_img()
