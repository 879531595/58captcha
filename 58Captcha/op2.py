import cv2 as cv
import numpy as np
import os

# kernel = cv.getStructuringElement(cv.MORPH_RECT,(10, 10))
kernel = cv.getStructuringElement(cv.MORPH_RECT,(2, 2))
#边缘
def edge_demo(image):
    blurred = cv.GaussianBlur(image, (3, 3), 0)
    gray = cv.cvtColor(blurred, cv.COLOR_RGB2GRAY)
    # xgrad = cv.Sobel(gray, cv.CV_16SC1, 1, 0) #x方向梯度
    # ygrad = cv.Sobel(gray, cv.CV_16SC1, 0, 1) #y方向梯度
    # edge_output = cv.Canny(xgrad, ygrad, 50, 150)
    edge_output = cv.Canny(gray, 50, 150)
    cv.imshow("Canny Edge", edge_output)
    dst = cv.bitwise_and(image, image, mask= edge_output)
    cv.imshow("Color Edge", dst)

#全局阈值
def threshold_demo(image):
    gray = cv.cvtColor(image, cv.COLOR_RGB2GRAY)  #把输入图像灰度化
    #直接阈值化是对输入的单通道矩阵逐像素进行阈值分割。
    ret, binary = cv.threshold(gray, 0, 255, cv.THRESH_BINARY | cv.THRESH_TRIANGLE)
    print("threshold value %s"%ret)
    cv.namedWindow("binary0", cv.WINDOW_NORMAL)
    cv.imshow("binary0", binary)

#局部阈值
def local_threshold(image):
    gray = cv.cvtColor(image, cv.COLOR_RGB2GRAY)  #把输入图像灰度化
    #自适应阈值化能够根据图像不同区域亮度分布，改变阈值
    binary =  cv.adaptiveThreshold(gray, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C,cv.THRESH_BINARY, 25, 10)

    cv.namedWindow("binary1", cv.WINDOW_NORMAL)
    cv.imshow("binary1", binary)

    cv.imwrite("test2.jpg", binary)

#用户自己计算阈值
def custom_threshold(image):
    gray = cv.cvtColor(image, cv.COLOR_RGB2GRAY)  #把输入图像灰度化
    h, w =gray.shape[:2]
    m = np.reshape(gray, [1,w*h])
    # mean = m.sum()/(w*h) * 1.24
    mean = m.sum()/(w*h)
    print("mean:",mean)
    ret, binary =  cv.threshold(gray, mean, 255, cv.THRESH_BINARY)
    cv.namedWindow("binary2", cv.WINDOW_NORMAL)
    # eroder = cv.erode(binary, kernel)
    # eroder = cv.dilate(binary, kernel)


    cv.imshow("binary2", binary)
    # cv.imwrite("test2.jpg", binary)
    # edge_demo(binary)
    # cv.imshow("binary2", binary)




# path = os.path.join(os.path.dirname(__file__), "test_A.png")


# src = cv.imread("captcha_img.jpg") # 亮度低

src = cv.imread("2_.jpg") # 亮度高
cv.namedWindow('input_image', cv.WINDOW_NORMAL) #设置为WINDOW_NORMAL可以任意缩放
cv.imshow('input_image', src)
threshold_demo(src)
local_threshold(src)
custom_threshold(src)
cv.waitKey(0)
cv.destroyAllWindows()


# import cv2
# import numpy as np
# img=cv2.imread(r"dome_1552634459000.png",0)
# x=cv2.Sobel(img,cv2.CV_16S,1,0)
# y=cv2.Sobel(img,cv2.CV_16S,0,1)
# absX=cv2.convertScaleAbs(x)#转回uint8
# absY=cv2.convertScaleAbs(y)
# dst=cv2.addWeighted(absX,1, absY,1,0.1)
# # cv2.imshow("absX",absX)
# # cv2.imshow("absY",absY)
# # eroder = cv.erode(dst, kernel)
# eroder = cv.dilate(dst, kernel)
# cv2.imshow("Result",eroder)
# cv2.waitKey(0)
# cv2.destroyAllWindows()



