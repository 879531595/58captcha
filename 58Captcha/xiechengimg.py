import cv2 as cv
import os
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

def op(path):
    img = Image.open(path).convert('L')
    # img = img.crop((13, 13, 108, 31))
    img = np.array(img)
    height = len(img)
    width = len(img[0])
    print('图片大小%dX%d' % (width, height))

    rows, cols = img.shape
    for i in range(rows):
        for j in range(cols):
            if (img[i, j] < 200):
                img[i, j] = 0
            else:
                img[i, j] = 1

    # plt.figure("love")
    plt.imshow(img, cmap='gray')
    plt.axis('off')
    plt.savefig("test3_.jpg")


op("test.jpg")
