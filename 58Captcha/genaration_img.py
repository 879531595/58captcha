

import os
import pygame

chinese_dir = 'out'
if not os.path.exists(chinese_dir):
    os.mkdir(chinese_dir)

pygame.init()
# start,end = (0x4E00, 0x9FA5) # 汉字编码范围

words = ["阿","斯","旺","大","坝"]
for word in words:
    font = pygame.font.Font("CNganKaiHK-Bold.ttf", 50)
    # 当前目录下要有微软雅黑的字体文件msyh.ttc,或者去c:\Windows\Fonts目录下找
    # 64是生成汉字的字体大小
    rtext = font.render(word, True, (0, 0, 0), (255, 255, 255))
    pygame.image.save(rtext, os.path.join(chinese_dir, word + ".png"))
